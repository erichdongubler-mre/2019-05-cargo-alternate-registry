~~The issues I've encountered with using alternate registries for Cargo have
happened while using the Cloudsmith repos. You can get your own free public
Cargo registries there for reproducing these issues.~~

- ~~Impossible to `package` only one crate from a workspace if something else
	depends on a crate that's not uploaded yet.~~

	- ~~Example:~~
		1. ~~Create your own repository. Don't upload any packages yet.~~
		2. ~~Comment out everything in the workspace `Cargo.toml` except for
		   `no-deps` and `alternate-registry-deps`.~~
		3. ~~Run `cargo package` in the `no-deps/` folder.~~

		~~I would expect this to Just Work™, since `alternate-registry-deps`
		conceptually seems to have nothing to do with the scope of `cargo
		package` in `no-deps/`.~~

		~~You can work around this by commenting out `"alternate-registry-deps"`
		from the workspace members in `Cargo.toml`.~~

	- ~~This also, strangely, problematic with the dependency chain
		`transitive-crates-io-deps -> crates-io-deps` despite calling `cargo
		package` in the `no-deps/` folder. Whaaat?~~

	**It seems that, as of 2019-05-01, this is a known problem with Cargo and
	is a flow that will hopefully be offered by the resolution of [this
	upstream Rust issue](https://github.com/rust-lang/cargo/issues/1169). In
	the meantime,
	[`cargo-publish-all`](https://gitlab.com/torkleyy/cargo-publish-all) seems
	to be a good interim solution for this! I was unfamiliar with publishing
	Cargo workspaces before this point. :)**

- ~~Even dependencies explicitly marked as being from the `crates-io` registry
	only seem to be searched for in the registry of a dependency.~~
	- ~~Example:~~
		1. ~~Create your own repository.~~
		2. ~~Comment out everything in the workspace `Cargo.toml` except for
			`crates-io-deps`.~~
		3. ~~Upload `crates-io-deps` to your repository.~~
		4. ~~Uncomment`transitive-crates-io-deps` from the workspace
		   `Cargo.toml`.~~

		~~I would expect this to Just Work™ since `crates-io` is specified for
		the `asdf` dependency of the `crates-io-deps` crate. However, it seems
		that the `cargo package` invocation for `transitive-crates-io-deps`
		only searches `problematic-crates`.~~

	**This issue was resolved on 2019-05-01 shortly after posting on
	[Reddit](https://www.reddit.com/r/rust/comments/bjk1qu/worlds_first_private_cargo_registry_rust_134/em9h4kk/).**
